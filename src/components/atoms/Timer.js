import React from 'react';
import styled from 'styled-components';

const StyledDiv = styled.div`
  background: white;
  height: 100px;
  width: 100px;
  border-radius: 50%;
  border: 4px solid blue;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 20px;
  font-weight: bold;
`;

const Timer = (props) => {
  return (
    <StyledDiv {...props} />
  );
};

export default Timer;