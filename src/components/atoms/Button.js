import React from 'react';
import styled from 'styled-components';

const StyledButton = styled.button`
  display: inline-block;
  border-radius: 4px;
  background-color: #f4511e;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 16px;
  padding: 10px;
  font-weight: bold;
  width: 200px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
  &:hover {
    opacity: 0.8;
  }
`;

const Button = (props) => {
  return (
    <StyledButton {...props} />
  );
};

export default Button;