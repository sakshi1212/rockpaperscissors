import React from 'react';
import styled from 'styled-components';

const StyledDiv = styled.div`
  border-radius: 50%;
  display: flex;
  justify-content: space-around;
  align-items: center;
  margin: 10px;
`;

const PlayerChoice = ({color, symbol, size, onClick}) => {
  const style = {
    backgroundColor: color,
    backgroundImage: symbol ? `url(./img/${symbol}.png` : '',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundOrigin: 'content-box',
    padding: size==='small' ? '12px' : '35px',
    height: size==='small' ? '75px' : '200px',
    width: size==='small' ? '75px' : '200px',
  }
  return (
    <StyledDiv style={style} onClick={onClick} />
  );
};

export default PlayerChoice;