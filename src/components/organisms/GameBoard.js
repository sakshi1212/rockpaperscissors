import React, { Component } from 'react';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';

import PlayerChoice from '../../components/molecules/PlayerChoice';
import Button from '../../components/atoms/Button';
import Timer from '../../components/atoms/Timer';


const GameWrapper = styled.div`
  background: beige;
  height: 600px;
  padding: 0px 20%;
`;

const ChoiceWrapper = styled.div`
  display: flex;
  justify-content: space-around;
  padding: 40px 80px;
`;

const PlayerChoiceOptions = styled.div`
`;

const PinkPlayerWrapper = styled.div`
  display: flex;
  justify-content: space-around;
`;

const Text = styled.p`
  font-size: 14px;
  font-weight: bold;
`

const WinnerText = styled(Text)`
  font-size: 16px;
  margin-top: 10px;
`

const StyledTimer = styled(Timer)`
  margin: 10px auto;
`

class GameBoard extends Component {
  constructor(props) {
    super(props);
    this.symbols = this.props.symbols;
    this.checkWinner = this.props.checkWinner;
    this.runGame = this.props.runGame;
    this.setPlayerPinkRock = this.props.setPlayerPinkRock;
    this.setPlayerPinkPaper = this.props.setPlayerPinkPaper;
    this.setPlayerPinkScissors = this.props.setPlayerPinkScissors;
  }

  render() {
    // console.log(this.props);
    return (
      <GameWrapper>
        <ChoiceWrapper>
          <PlayerChoice symbol={this.props.state.playerBlue} color="lightskyblue" />
          <PinkPlayerWrapper>
            <PlayerChoice symbol={this.props.state.playerPink} color="pink" />
            {this.props.mode===2 && 
              <PlayerChoiceOptions>
                <PlayerChoice symbol={"rock"} color="pink" size="small" onClick={this.setPlayerPinkRock} />
                <PlayerChoice symbol={"paper"} color="pink" size="small" onClick={this.setPlayerPinkPaper} />
                <PlayerChoice symbol={"scissors"} color="pink" size="small" onClick={this.setPlayerPinkScissors} />
              </PlayerChoiceOptions>
            }
          </PinkPlayerWrapper>
        </ChoiceWrapper>
        {this.props.mode===2 && 
          <React.Fragment>
            <StyledTimer>
              {this.props.state.countdown}
            </StyledTimer>
            <Text> Click 'Run Game' and make your choice before the timer runs out !</Text>
          </React.Fragment>
        }
        <WinnerText>{this.props.state.winner}</WinnerText>
        <Button onClick={this.runGame}>Run Game</Button>
        <Button onClick={() => this.props.history.push('/')}>Change Game Mode</Button>
      </GameWrapper>
    );
  }
}

export default withRouter(GameBoard);