import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import Home from './pages/Home';
import CompVsCompGame from './pages/ComputerVsComputer';
import CompVsPlayerGame from './pages/ComputerVsPlayer';

class App extends Component {
  constructor(props) {
    super(props);
    this.symbols = ["rock", "paper", "scissors"];
  }

  checkWinner = ({ playerBlue, playerPink }) => {
    if (!playerBlue || !playerPink) {
      return 'Invalid Game, Please run game again !';
    }
    if (playerBlue === playerPink) {
      return 'It\'s a draw !';
    }
    if ((playerBlue === "rock" && playerPink ==="scissors") || (playerBlue === "paper" && playerPink === "rock") || (playerBlue === "scissors" && playerPink === "paper")) {
      return 'Player Blue Wins !';
    }
    return 'Player Pink Wins !';
  }

  render() {
    const App = () => (
      <div>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route
            path='/compVsComp'
            render={(props) => <CompVsCompGame {...props} checkWinner={this.checkWinner} symbols={this.symbols} />}
          />
          <Route
            path='/compVsPlayer'
            render={(props) => <CompVsPlayerGame {...props} checkWinner={this.checkWinner} symbols={this.symbols} />}
          />
        </Switch>
      </div>
    )
    return (
      <Switch>
        <App/>
      </Switch>
    );
  }
}

export default App;