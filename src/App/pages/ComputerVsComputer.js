import React, { Component } from 'react';

import GameBoard from '../../components/organisms/GameBoard';

class Game extends Component {

  constructor(props) {
    super(props);
    this.state = {};
    this.symbols = this.props.symbols;
    this.checkWinner = this.props.checkWinner;
  }

  runGame = () => {
    let count = 0;
    let animationInterval = setInterval(() => {
      count++;
      this.setState({
        playerBlue: this.symbols[Math.floor(Math.random()*3)],
        playerPink: this.symbols[Math.floor(Math.random()*3)],
        winner: '',
      })
      if (count > 15) {
        clearInterval(animationInterval);
        this.setState({ winner: this.checkWinner(this.state) });
      }
    }, 80);
  }

  render() {
    return (
      <div className="App">
        <h1>Computer vs Computer</h1>
        <GameBoard 
          mode={1}
          state={this.state}
          setState={this.setState}
          runGame={this.runGame}
          checkWinner={this.checkWinner}
          symbols={this.symbols}
        />
      </div>
    );
  }
}

export default Game;