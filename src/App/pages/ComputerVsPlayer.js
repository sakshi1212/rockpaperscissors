import React, { Component } from 'react';

import GameBoard from '../../components/organisms/GameBoard';

class Game extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.symbols = this.props.symbols;
    this.checkWinner = this.props.checkWinner;
  }

  runGame = () => {
    this.setState({
      countdown: 10,
      winner: '',
      playerBlue: null,
      playerPink: null,
    })
    let animationInterval = setInterval(() => {
      let count = this.state.countdown - 1;
      this.setState({
        playerBlue: this.symbols[Math.floor(Math.random()*3)],
        winner: '',
        countdown: count,
      })
      if (count === 0) {
        clearInterval(animationInterval);
        this.setState({ 
          winner: this.checkWinner(this.state) 
        });
      }
    }, 200);
  }

  setPlayerPinkRock = (e) => {
    this.setState({
      playerPink: "rock",
    })
  }

  setPlayerPinkPaper = (e) => {
    this.setState({
      playerPink: "paper",
    })
  }

  setPlayerPinkScissors = (e) => {
    this.setState({
      playerPink: "scissors",
    })
  }

  render() {
    return (
      <div className="App">
        <h1>Computer vs Player</h1>
        <GameBoard 
          mode={2}
          state={this.state}
          setState={this.setState}
          runGame={this.runGame}
          checkWinner={this.checkWinner}
          symbols={this.symbols}
          setPlayerPinkRock={this.setPlayerPinkRock}
          setPlayerPinkPaper={this.setPlayerPinkPaper}
          setPlayerPinkScissors={this.setPlayerPinkScissors}
        />
      </div>
    );
  }
}

export default Game;