import React, { Component } from 'react';
import Button from '../../components/atoms/Button';
import styled from 'styled-components';

const ButtonWrapper = styled.div`
  background: antiquewhite;
  height: 600px;
  display: flex;
  align-items: center;
  justify-content: center;
`

class Home extends Component {
  render() {
    const {
      history
    } = this.props;

    return (
      <div className="App">
        <h1>Rock Paper Scissors</h1>
        <ButtonWrapper>
          <Button onClick={() => history.push('/compVsPlayer')}>
            Player vs Computer
          </Button>
          <Button onClick={() => history.push('/compVsComp')}>
            Computer vs Computer
          </Button>
        </ButtonWrapper>
      </div>
    );
  }
}
export default Home;