# rockPaperScissors

### Summary

Simple Rock-Paper-Scissors game that allows you to play in 2 modes 
1. Computer vs Computer
2. Player vs Computer

### Pre-requistes
 
1. React ^16.9.0  

### Local Installation and Start Client  
1. Clone project and cd /into/project/folder
1. run `npm install` to install the libraries
2. `npm run start` to start.   

The app is now accessible at http://localhost:3000  



